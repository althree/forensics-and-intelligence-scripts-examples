#!/usr/bin/env python
import sys
import getopt
import string
import argparse
import json
from stix2 import FileSystemSource
from stix2 import Filter

# This utility connects to the stix2 filesystem representing the MITRE Att&CK framework
# then export the data queried.

# First you need to download the stix2 file system copy of mitre as described here:
# https://github.com/mitre/cti/blob/master/USAGE.md
#
# IF the cti folder does not exists: clone it here from https://github.com/mitre/cti.git
# IF the cti folder exists cd into it and update your local copy

def main():
    argp = argparse.ArgumentParser()

    argp.add_argument('-i', action='store', dest='fs',
                      help='The file system source you want to parse with stix2')
    #Normally you should use './cti/attack-entreprise' here

    argp.add_argument('-o', action='store', dest='of',
                      help='The output JSON file')

    params = argp.parse_args()
    of = params.of
    fs = params.fs

    mitre_dict = get_mitre_techniques_dict(fs)
    with open(of, 'w') as output_file:
        json.dump(mitre_dict, output_file)

def get_mitre_techniques_dict(fileSysSource)

    fsrc = FileSystemSource(fileSysSource)
    filt = Filter("type", "=", "attack-pattern")
    techniques = fsrc.query([filt])

    mitre_dict["techniques"] = []

    for t in techniques:
        d = {}

        d["external_id"] = t.get("external_references")[0].get("external_id")
        d["name"] = t.name
        d["url"] = t.get("external_references")[0].get("url")
        d["t_bypass_defenses"] = t.get("x_mitre_defense_bypassed")
        d["t_data_sources"] = t.get("x_mitre_data_sources")
        d["t_platforms"] = t.get("x_mitre_platforms")
        #d["t_detection"] = t.get("x_mitre_detection") #Removed as cannot sent it by mail
        #d["t_description"] = t.get("description") # IDEM
        d["kill_chain_phases"] = [ p.phase_name for p in t.get("kill_chain_phases")]

        mitre_dict["techniques"].append(d)

    return mitre_dict

if __name__ == "__main__":
    main()

#!/usr/bin/env python
import urllib.request
import argparse
import xml.etree.ElementTree as ET

# Script that turn IPs from masscan result into urls
# stored in a file

if __name__ == "__main__":

    argp = argparse.ArgumentParser()
    argp.add_argument('-f', action='store', dest='scanfile',
        help='The xml scan file')
    argp.add_argument('-o', action='store', dest='outfile',
        help='The output file')
    argp.add_argument('-P', action='store', dest='protocol',
        help='Choose between http, https, ...')
    argp.add_argument('-n', action='store', dest='port',
        help='The port to add to the url')
    params = argp.parse_args()

    xml_scan_file = params.scanfile
    outfile = params.outfile
    protocol = params.protocol
    port = params.port

    urls = [] # Here we parse the IPs from the xml

    if xml_scan_file != None:
        try:
            root = ET.parse(xml_scan_file).getroot()
            for type_tag in root.findall('host/address'):
                ip = type_tag.get('addr')

                if protocol == None:
                    # If no flag is defined in params fallback to http
                    protocol = "http"

                if port != None:
                    url = protocol + "://" + ip + ":" + str(port)
                else:
                    url = protocol + "://" + ip

                print(url)
                urls.append(url)

        except:
            print("[Error] : There was a problem scnning your scan file")
            exit()
    else:
        print("[Error] : You did not provide a scanfile")
        exit()

    url_num = 0

    f = open(outfile,"w+")
    for u in urls:
        url_num += 1
        f.write( u + "\n")

    f.close()

    print(">>> DONE : " + str(url_num) + " wrote in the output file.")

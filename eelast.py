#!/usr/bin/python3

from elasticsearch import Elasticsearch
from datetime import datetime
import argparse
import sys
import re
import json


if __name__ == "__main__":

    descr="""
        Redirect your stdout to an elastic server using this script + piping

        This program will help you index documents on an elastic server. You
        can do it by piping your stdout to this utility:

            " cat afile.json | eelast.py -s foo.bar -i quxindex -J -D "

        If you want to index a raw txt file or output you can do it by ommitting
        the -J param. This will wrap your text in a JSON object with 2 keys:
        time, rawtext. In this case use the comand :

            "cat afile.txt | eelast.py -s foo.bar -i quxindex -D"

    """

    argp = argparse.ArgumentParser(description=descr)
    argp.add_argument('-s', action='store', dest='server',
        help='The elastic server IP or hostname')
    argp.add_argument('-i', action='store', dest='index',
        help='The index where to index the output')

    params = argp.parse_args()

    elastic_server = params.server
    elastic_index = params.index
    doc = sys.stdin

    if elastic_server != None:

        if elastic_index != None:

            es=Elasticsearch([{'host': elastic_server,'port':9200}])

            out = ""
            for i in doc:
                out += i

            """
            out2 = re.findall(r'\{.*\}', out)

            for j in out2:
                try:
                    ind = es.index(index=elastic_index, doc_type='text', body=j)
                except:
                    pass
            """
            # current date and time


            dic = {
                "@timestamp" : datetime.now().isoformat(),
                "raw_data" : out
            }

            ind = es.index(index=elastic_index, doc_type='text', body=dic)

        else:
            print('Please provide an elastic index.')
            exit()

    else:
        print('Please provide an elastic server to connect to.')
        exit()

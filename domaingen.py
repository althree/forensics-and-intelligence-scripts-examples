import argparse
import string 
import random 

def main():

    argp = argparse.ArgumentParser()
    argp.add_argument('-l', action='store', dest='length', help='How many chars do you want in your domain')
    argp.add_argument('-e', action='store', dest='extension', help='The tld to use like ".com" ".br" ...')
    argp.add_argument('-n', action='store', dest='amount', help='how many domains do yo want to generate')
    argp.add_argument('-i', action='store', dest='inputf', help='input file to transform words in domains')

    
    params = argp.parse_args()

    if params.length:
        length = int(params.length)

    tld = str(params.extension)
    amount = int(params.amount)
    inputf = str(params.inputf)

    if params.length:
        if amount > 1:
            for i in range(amount):
                res = ''.join(random.choices(string.ascii_lowercase, k = length))
                print(res + "." + tld)
        else:
            res = ''.join(random.choices(string.ascii_lowercase, k = length))
            print(res + "." + tld)

    if params.inputf:
        with open(inputf) as f:
            if not amount:
                try:
                    for line in f:
                        print(line - "\n" + "." + tld)
                except:
                    pass
                    
            elif amount > 1:
                for i in range(amount):
                    try:
                        for line in f:
                            print(line - "\n" + "." + tld)
                    except:
                        pass
            else:
                for line in f:
                    print(line - "\n" + "." + tld)
                    break
                        
if __name__ == '__main__':
    main()

#!/usr/bin/env python
import getpass
import http.client
import json
import os
import sys

class Auth0Checker:
# uses auth0.com api
# log in to https://auth0.com/signals/ip to check available "hits"
# maximum of 40000 "hits" per 24hours, reset time is also mentioned
# username: vulnerablestuff_3333@tinkmail.net
# password: VulnerableStuff_3333
# API key:
# Getting full JSON report on the IP
#def check_ip(ip):
#    conn = http.client.HTTPSConnection('signals.api.auth0.com')
#    headers = { 'X-Auth-Token': "" }
#    conn.request("GET", "/v2.0/ip/" + ip, headers=headers)
#    resp = conn.getresponse()
#    data = resp.read()
#    conn.close()

#    parsed_dict = json.loads(data)
#    parsed_json = json.dumps(parsed_dict, indent=2, sort_keys=True)
#    print(parsed_json)
##   print(data.decode("utf-8"))


    def __init__(self, auth_token):
        self.x_auth_token = auth_token
        self.auth0_api_host = 'signals.api.auth0.com'

    def screener(self):

        print("\n")
        print("\n")
        art = r'''

                  \.   \.      __,-"-.__      ./   ./
               \.   \`.  \`.-'"" _,="=._ ""`-.'/  .'/   ./
                \`.  \_`-''      _,="=._      ``-'_/  .'/
                 \ `-',-._   _.  _,="=._  ,_   _.-,`-' /
              \. /`,-',-._"""  \ _,="=._ /  """_.-,`-,'\ ./
               \`-'  /    `-._  "       "  _.-'    \  `-'/
               /)   (         \    ,-.    /         )   (\
            ,-'"     `-.       \  /   \  /       .-'     "`-,
          ,'_._         `-.____/ /  _  \ \____.-'         _._`,
         /,'   `.                \_/ \_/                .'   `,\
        /'       )                  _                  (       `\
                /   _,-'"`-.  ,++|T|||T|++.  .-'"`-,_   \
               / ,-'        \/|`|`|`|'|'|'|\/        `-, \
              /,'             | | | | | | |             `,\
             /'               ` | | | | | '               `\
                                ` | | | '
                                  ` | '


         '''
        print(art)
        print("\n")
        print("\n")

    # def functions for options
    # until 'Install Certificates.command' in the Python folder was run
    #check if IP appears on any of the blacklists of Auth0 Signals
    def check_ip(self,ip):
        conn = http.client.HTTPSConnection(self.auth0_api_host)
        headers = { 'X-Auth-Token': self.x_auth_token }
        conn.request("GET", "/badip/" + ip, headers=headers)
        resp = conn.getresponse()
        data = resp.read()
        conn.close()

        if "200" in str(data):
            print("\n")
            print("THAT IP ADDRESS APPEARS IN AT LEAST ONE BLACKLIST!")
            print("\n")
            return True
        else:
            print("\n")
            print("THAT IP ADDRESS DOES NOT APPEAR IN ANY OF THE BLACKLISTS!")
            print("\n")
            return False


    def check_domain(self, domain):
        conn = http.client.HTTPSConnection(self.auth0_api_host)
        headers = {
            'X-Auth-Token': self.x_auth_token,
            'Accept': "application/json"
        }
        conn.request("GET", "/baddomain/" + domain, headers=headers)
        resp = conn.getresponse()
        data = resp.read()
        conn.close()

        if '{"domain": {"blacklist": []' in str(data):
            print("\n")
            print("THAT DOMAIN DOES NOT APPEAR IN ANY OF THE BLACKLISTS!")
            print("\n")
            return True

        else:
            print("\n")
            print("THAT DOMAIN APPEARS IN AT LEAST ONE BLACKLIST!")
            print("\n")
            return False

    def check_email(self, email):
        conn = http.client.HTTPSConnection(self.auth0_api_host)
        headers = {
            'X-Auth-Token': self.x_auth_token,
            'Accept': "application/json"
        }
        conn.request("GET", "/bademail/" + email, headers=headers)
        resp = conn.getresponse()
        data = resp.read()
        conn.close()

        if '{"email": {"blacklist": []' in str(data):
            print("\n")
            print("THAT EMAIL ADDRESS DOES NOT APPEAR IN ANY OF THE BLACKLISTS!")
            return True

        elif '"domain": {"blacklist": []' not in str(data):
            print("HOWEVER, THE DOMAIN NAME APPEARS IN AT LEAST ONE BLACKLIST!")
            print("\n")

        else:
            print("\n")
            print("THAT EMAIL ADDRESS APPEARS IN AT LEAST ONE BLACKLIST!")
            print("\n")


if __name__=='__main__':

    auth_token = ''

    # JUST TO CLEAR THE SCREEN ON THE CONSOLE
    clear = lambda: os.system('clear') #Modify it to suit your OS specificities
    clear()

    a0 = Auth0Checker(auth_token)

    try:
        while True:
            a0.screener()
            print("Choose option from the menu and press ENTER:")
            print("\n")
            print("1. Check IP address")
            print("2. Check domain name")
            print("3. Check email address")
            print("\n")
            print("[Your Choice 1-3]")
            menu_option=int(input())

            if menu_option == 1:
                ip=input("Enter IP address : ")
                a0.check_ip(ip)
                input("Press Enter to continue...")
                clear()
            elif menu_option == 2:
                domain=input("Enter domain name : ")
                a0.check_domain(domain)
                input("Press Enter to continue...")
                clear()
            elif menu_option == 3:
                email=input("Enter email address : ")
                a0.check_email(email)
                input("Press Enter to continue...")
                clear()
            else:
                print("Invalid menu option!")
                print("\n")
                clear()

    except KeyboardInterrupt:
        print("Bye!")
        sys.exit()

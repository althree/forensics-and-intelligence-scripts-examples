#!/usr/bin/env python
import ipaddress
import urllib.request
import argparse
import time
import random
from prettytable import PrettyTable
from torrequest import TorRequest
from requests.adapters import HTTPAdapter
import threading
from multiprocessing import Process
import xml.etree.ElementTree as ET


test_strings = ["Index of", "Parent Directory"]
res = []

def try_url(ip,torsess):
    try:
        url = "http://" + str(ip)
        response = torsess.get(url)
        if response.status_code == 200:
            if any(s in response.text for s in test_strings):
                res.append(url)
                print("[INFO] : Found URL > " + url)

            else:
                return
        else:
            return
    except:
        pass


if __name__ == "__main__":

    argp = argparse.ArgumentParser()
    argp.add_argument('-f', action='store', dest='scanfile',
        help='The xml scan file')
    argp.add_argument('-o', action='store', dest='outfile',
        help='The xml scan file')
    params = argp.parse_args()

    xml_scan_file = params.scanfile
    outfile = params.outfile

    ips = [] # Here we parse the IPs from the xml

    if xml_scan_file != None:
        try:
            root = ET.parse(xml_scan_file).getroot()
            for type_tag in root.findall('host/address'):
                ip = type_tag.get('addr')
                ips.append(ip)
        except:
            print("[Error] : There was a problem scanning your scan file")
            exit()
    else:
        print("[Error] : You did not provide a scanfile")
        exit()

    ip_num = 0

    for i in ips:
        ip_num += 1

    print("[INFO] : We will look into " + str(ip_num) + " IP(s) addresse(s) for malware repos")

    threads = []

    with TorRequest() as tr:
        tr.session.mount("http://", HTTPAdapter(max_retries=1))
        for ip in ips:
            threads.append(
                threading.Thread(target=try_url, args=(ip,tr))
            )
            threads[-1].start()

        for t in threads:
            t.join()

    f = open(outfile,"w+")
    for i in res:
        f.write( i + "\n")
    f.close()

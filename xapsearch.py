#!/usr/bin/env python

import sys
import xapian
import argparse

def search(db, term):
    
    try:
        database = xapian.Database(str(db))

        enquire = xapian.Enquire(database)

        query_string = str(term)

        qp = xapian.QueryParser()
        stemmer = xapian.Stem("english")
        qp.set_stemmer(stemmer)
        qp.set_database(database)
        qp.set_stemming_strategy(xapian.QueryParser.STEM_SOME)
        query = qp.parse_query(query_string)

        enquire.set_query(query)
        matches = enquire.get_mset(0, 10)

        print("%i results found." % matches.get_matches_estimated())
        print("Results 1-%i:" % matches.size())

        for m in matches:
            print("%i: %i%% docid=%i [%s]" % (m.rank + 1, m.percent, m.docid, m.document.get_data()))

    except Exception:
        sys.exit(1)

if __name__ == "__main__":

    argp = argparse.ArgumentParser()
    argp.add_argument('-d', action='store', dest='db',
        help='The xapian DB to query')
    argp.add_argument('-s', action='store', dest='term',
        help='The term to look for')
    params = argp.parse_args()

    db = params.db
    term = params.term

    if db:
        if term:
            search(db, term)
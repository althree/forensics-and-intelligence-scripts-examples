#!/usr/bin/env python

# This utility takes a yaml file describing wich techniques should be put in
# which color and trnasforms it in a JSON file to be imported in the
# MITRE navigator.

import yaml
import JSON
import argparse

def main():
    argp = argparse.ArgumentParser()

    argp.add_argument('-y', action='store', dest='yamlFile',
                      help='The yaml file containing your techniques to be put in the navigator')

    argp.add_argument('-i', action='store', dest='fs',
                      help='The file system source you want to parse with stix2')
    #Normally you should use './cti/attack-entreprise' here

    argp.add_argument('-o', action='store', dest='of',
                      help='The output JSON file')

    params = argp.parse_args()
    yml = params.yamlFile
    of = params.of
    fs = params.fs

def parse_ymlFile(ymlFile):
    """
    A simple function that parses the Yaml file that includes the different
    techniques to be put on the ATT&CK navigator.

    The file should be formatted like that:
        - A section named 'techniques' containing the techniques to be put on
        the navigator. This section contains subsections. One section by color.
        Name subsections with the hex code of the color without the '#'.
        Example:

            - techniques:
                - cccccc:
                    - T1218
                    - T1514
                    - T1118
                - ffffff:
                    - Txxxx
                    - Txxxx
    """
    
with open(yml) as ymlFile:
    documents = yaml.full_load(ymlFile)
    for item, doc in documents.items():
        print(item, ":", doc)

#!/usr/bin/env bash

while true
do

clear

echo -e "\e[44mSSH Attempts and Active Connections"
echo -e "\e[0m"
ss | grep -i ssh | column -t
printf "\n"

echo -e "\e[44mSSH Connections Currently Opened"
echo -e "\e[0m"
last -a | grep still | column -t
printf "\n"

echo -e "\e[44mUsers Curently Connected to the System"
echo -e "\e[0m"
w | column -t
printf "\n"

sleep 60

done

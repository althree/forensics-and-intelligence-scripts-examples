#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pymisp import PyMISP
from keys import misp_url, misp_key
import requests

url = 'https://www.circl.lu/doc/misp/feed-osint/'
osintcircl = requests.get('{}manifest.json'.format(url))

def init(url, key):
    return PyMISP(url, key, False, 'json')

if __name__ == '__main__':

    """
    parser = argparse.ArgumentParser(description='Create an event on MISP.')
    parser.add_argument("-e",
                        "--event",
                        type=int,
                        action='store',
                        dest='event',
                        help="The id of the event to update.")

    args = parser.parse_args()
    """

    misp = init(misp_url, misp_key)


    for uri in osintcircl.json():
            try:
                req = requests.get('{}{}.json'.format(url,uri))
                misp.add_event(req.json())
            except:
                pass

#!/usr/bin/env python

import os, sys, getopt
from os import walk
from filehash import FileHash
import argparse
import json
import exiftool
from zipfile import ZipFile
from rarfile import RarFile
import time
from pefile import PE
from mmap import mmap, PROT_READ
import re

# Thanks Didier Stevens for the regex
dLibrary = {
            'email': r'[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}',
            'email-domain': r'[a-zA-Z0-9._%+-]+@([a-zA-Z0-9.-]+\.[a-zA-Z]{2,6})',
            'url': r'[a-zA-Z]+://[-a-zA-Z0-9.]+(?:/[-a-zA-Z0-9+&@#/%=~_|!:,.;]*)?(?:\?[-a-zA-Z0-9+&@#/%=~_|!:,.;]*)?',
            'url2' : r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+',
            'url-domain': r'[a-zA-Z]+://([-a-zA-Z0-9.]+)(?:/[-a-zA-Z0-9+&@#/%=~_|!:,.;]*)?(?:\?[-a-zA-Z0-9+&@#/%=~_|!:,.;]*)?',
            'ipv4': r'\b(?:(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.){3}(?:25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\b',
            'str': r'"[^"]+"',
            'str-e': r'"[^"]*"',
            'str-u': r'"([^"]+)"',
            'str-eu': r'"([^"]*)"',
            'btc': r'(?#extra=P:BTCValidate)\b[13][a-km-zA-HJ-NP-Z1-9]{25,34}\b',
            'onion': r'[a-zA-Z2-7]{16}\.onion',
           }

def strings(fname, n=6):
    """
    This function list strings in a PE file.
    """
    matches = []
    with open(fname, 'rb') as f, mmap(f.fileno(), 0, prot=PROT_READ) as m:
        for match in re.finditer(('([\w/]{%s}[\w/]*)' % n).encode(), m):
            yield match.group(0)


def searchPattern(file, pattern):
    """
    This function search for matches on one of the patterns in dLibrary
    """
    pattern = re.compile(dLibrary[pattern])
    foundPatterns = []

    for line in strings(file):
        print(line)
        if pattern.match(line):
            foundPatterns.append(line.decode("utf-8"))

    return foundPatterns


def dir_path(string):
    if os.path.isdir(string):
        return string
    else:
        raise NotADirectoryError(string)

'''
    For the given path, get the List of all files in the directory tree
'''
def getListOfFiles(dirName):
    # create a list of file and sub directories
    # names in the given directory
    listOfFile = os.listdir(dirName)
    allFiles = list()
    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = os.path.join(dirName, entry)

        # If entry is a directory then get the list of files in this directory
        if os.path.isdir(fullPath):
            allFiles = allFiles + getListOfFiles(fullPath)
        else:
            if entry.split(".")[-1] == "exe":
                allFiles.append(fullPath)

    return allFiles


def pereport():

    argp = argparse.ArgumentParser()
    argp.add_argument('-p', type=dir_path, action='store', dest='path',
        help='The folder where the PEs you want to analyse are.')

    params = argp.parse_args()
    path = str(params.path)
    files = getListOfFiles(path)
    timestr = time.strftime("%Y%m%d-%H%M%S")
    fleName = "PE-report-" + timestr
    reportPath = path + fleName
    fle = open(reportPath, "a")
    totHashes = []

    # Header of the report
    fle.write("Report Time : " + timestr + "\n")
    fle.write("Num of files analysed : " + str(len(files))  + "\n\n")
    fle.write("---- BEGIN OF REPORT ----\n\n")

    for f in files:
        print("Analysis for file : " + f.split("/")[-1])
        fle.write("Analysis for file : " + f.split("/")[-1] + "\n")

        # try:
        pe = PE(f)
        print("Hash of the file : " + pe.get_imphash())
        fle.write("Hash of the file : " + pe.get_imphash() + "\n")
        totHashes.append(pe.get_imphash())

        print("=========================================\n\n")
        fle.write("=========================================\n\n")


        print("Analyis of DLL imports\n")
        fle.write("Analyis of DLL imports\n")
        fle.write("----\n\n")


        for d in pe.DIRECTORY_ENTRY_IMPORT:
            print("DLL : " + d.dll.decode('UTF-8'))
            fle.write("DLL : " + d.dll.decode('UTF-8') + "\n")
            print("=====")
            fle.write("====\n")

            try:
                for imp in d.imports:
                    print("import : " + imp.name.decode('UTF-8'))
                    fle.write("import : " + imp.name.decode('UTF-8') + "\n")
            except:
                pass


            print("=====\n")
            fle.write("====\n\n")

            print("Trying to find interesting urls in the file\n")
            print("=====\n")

            for line in strings(f):

                patternUrl = re.compile(dLibrary["url"])
                patternEmail = re.compile(dLibrary["email"])
                patternIpv4 = re.compile(dLibrary["ipv4"])
                patternBtc = re.compile(dLibrary["btc"])
                patternTor = re.compile(dLibrary["onion"])

                if patternUrl.match(line.decode("utf-8")):
                    fle.write(line.decode("utf-8") + "\n")
                    print(line.decode("utf-8"))

                if patternEmail.match(line.decode("utf-8")):
                    fle.write(line.decode("utf-8") + "\n")
                    print(line.decode("utf-8"))

                if patternIpv4.match(line.decode("utf-8")):
                    fle.write(line.decode("utf-8") + "\n")
                    print(line.decode("utf-8"))

                if patternBtc.match(line.decode("utf-8")):
                    fle.write(line.decode("utf-8") + "\n")
                    print(line.decode("utf-8"))

                if patternTor.match(line.decode("utf-8")):
                    fle.write(line.decode("utf-8") + "\n")
                    print(line.decode("utf-8"))

        # except:
        #     pass

    fle.write(str(len(totHashes)) + " hashes generated\n")
    fle.write("=======================================\n")
    for h in totHashes:
        fle.write(h + "\n")

    fle.write("---- END OF REPORT ----\n\n")
    print("Report written in : " + reportPath)

if __name__ == '__main__':
    pereport()

#!/usr/bin/env python
import os, sys, getopt
from os import walk
from filehash import FileHash
import argparse
import json
import exiftool
from zipfile import ZipFile
from rarfile import RarFile
import time

def dir_path(string):
    if os.path.isdir(string):
        return string
    else:
        raise NotADirectoryError(string)

'''
    For the given path, get the List of all files in the directory tree
'''
def getListOfFiles(dirName):
    # create a list of file and sub directories
    # names in the given directory
    listOfFile = os.listdir(dirName)
    allFiles = list()
    # Iterate over all the entries
    for entry in listOfFile:
        # Create full path
        fullPath = os.path.join(dirName, entry)
        # If entry is a directory then get the list of files in this directory
        if os.path.isdir(fullPath):
            allFiles = allFiles + getListOfFiles(fullPath)
        else:
            allFiles.append(fullPath)

    return allFiles

def allist():

    argp = argparse.ArgumentParser()
    argp.add_argument('-p', type=dir_path, action='store', dest='path',
        help='The folder where the files you want to extract metadata')
    argp.add_argument('-k', action='store', dest='key',
        help='If you have one the key to unzip zipped archives')

    params = argp.parse_args()

    path = str(params.path)
    zipKey = str(params.key)

    files = getListOfFiles(path)
    hashes = []

    timestr = time.strftime("%Y%m%d-%H%M%S")
    fleName = "allist-report-" + timestr
    extrFleName = "extracts-" + fleName
    reportPath = path + fleName
    extractPath = path + extrFleName

    fle = open(reportPath, "a")

    # Header of the report
    fle.write("Report Time : " + timestr + "\n")
    fle.write("Num of files scanned : " + str(len(files))  + "\n")
    if zipKey != "":
        fle.write("Password used for archives carving : " + str(zipKey)  + "\n")
        extractDir = os.mkdir(extractPath, 0o777)
        fle.write("Files extracted in : " + extractPath  + "\n")

    fle.write("---- BEGIN OF REPORT ----\n\n")

    for f in files:
        md5hasher = FileHash("md5")
        h = md5hasher.hash_file(f)
        complPath = f

        print("FileHash : " + str(h))
        fle.write("FileHash : " + str(h) + "\n")
        hashes.append(str(h))

        with exiftool.ExifTool() as ex:
            meta = ex.get_metadata(f)

            for k, v in meta.items():
                print(k, v)
                fle.write(str(k) + " : " + str(v) + "\n")

                if v == "ZIP":
                    print("[Found a ZIP file] : trying to list the contained files")
                    fle.write("[Found a ZIP file] : trying to list the contained files")

                    with ZipFile(complPath, 'r') as zipObj:
                        listOfiles = zipObj.infolist()

                    for elem in listOfiles:
                        print("File found   : " + elem.filename)
                        fle.write("File found   : " + elem.filename + "\n")
                        print("File size    : " + str(elem.file_size))
                        fle.write("File size    : " + str(elem.file_size) + "\n")
                        print("File date    : " + str(elem.date_time))
                        fle.write("File date    : " + str(elem.date_time) + "\n")
                        print("--\n")
                        fle.write("--\n")

                    # We will try to unzip with the password (zipKey) if there is no
                    # pass set, we try to unzip if it fails we do not cras by using
                    # "pass"

                    if zipKey is not "":
                        try:
                            z = ZipFile(complPath)
                            z.extractall(path=extractPath, pwd=bytes(zipKey, 'utf-8'))
                        except:
                            pass
                    else:
                        try:
                            z = ZipFile(complPath)
                            z.extractall(path=extractPath)
                        except:
                            pass



                if str(v).split(".")[1] == "rar":
                    print("[Found a RAR file] : trying to list the contained files")
                    if zipKey is not "":
                        try:
                            r = RarFile(complPath)
                            r.extractall(path=extractPath, pwd=bytes(zipKey, 'utf-8'))
                        except:
                            pass
                    else:
                        try:
                            r = ZipFile(complPath)
                            r.extractall(path=extractPath)
                        except:
                            pass
                            #fle.write("[Found a ZIP file] : trying to list the contained files")

        print("\n---\n")
        fle.write("\n---\n")

    print(str(len(hashes)) + " file hashes found : ")
    fle.write(str(len(hashes)) + " file hashes found : \n\n")

    for hash in hashes:
        print(hash)
        fle.write(hash + "\n")

    fle.write("\n---\n")
    fle.write("---- END OF REPORT ----\n\n")


    print("[INFO] : Saved results file - " + fleName + "\n")
    fle.close()



if __name__ == '__main__':

    allist()
